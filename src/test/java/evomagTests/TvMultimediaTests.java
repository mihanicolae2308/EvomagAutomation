package evomagTests;

import models.ProductCategoriesModel;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.PhonesAndAccessoriesPage;
import pages.TvMultimediaPage;
import utils.BaseTest;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Class for testing TV and Multimedia page.
 * @author Mihai Nicolae, 04-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class TvMultimediaTests extends BaseTest {
    @DataProvider(name = "CSVTvMultimediaDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a generic model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("tvAndMultimediaTestData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            ProductCategoriesModel model = new ProductCategoriesModel();
            model.setWebElementName(csvData.get(i)[0]);
            model.setExpectedUrl(csvData.get(i)[1]);
            dp[i][0] = model;
        }
        return dp;
    }

    @Test(dataProvider = "CSVTvMultimediaDataProvider")
    /**
     * Method for running tests on TV and Multimedia page using test data from a CSV data provider
     * @param   pcm a generic model that is used for this page
     * @see     ProductCategoriesModel
     */
    public void testTvAndMultimedia(ProductCategoriesModel pcm) {
        driver.navigate().to("https://www.evomag.ro/TV-Multimedia/");
        TvMultimediaPage pc = PageFactory.initElements(driver, TvMultimediaPage.class);

        Object obj = pc;

        for (Field field : pc.getClass().getFields()) {
            if (field.getName().equals(pcm.getWebElementName()) && (field.getType() == WebElement.class)) {
                try {
                    WebElement el = (WebElement) field.get(obj);
                    pc.elementClick(el);
                    String currentURL = driver.getCurrentUrl();
                    Assert.assertEquals(currentURL, pcm.getExpectedUrl());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
