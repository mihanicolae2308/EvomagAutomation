package evomagTests;

import models.ProductCategoriesModel;
import models.ShoppingBasketModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.MainPage;
import utils.BaseTest;

import java.io.File;
import java.util.List;

/**
 * Class for testing shopping basket actions.
 * @author Mihai Nicolae, 10-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class ShoppingBasketTests extends BaseTest {
    @DataProvider(name = "CSVAddToShoppingBasketDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a model for the tested scenario.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("addToShoppingBasket.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            ShoppingBasketModel model = new ShoppingBasketModel();
            model.setCategory(csvData.get(i)[0]);
            model.setCategoryPageUrl(csvData.get(i)[1]);
            model.setSubcategory(csvData.get(i)[2]);
            model.setSubcategoryPageUrl(csvData.get(i)[3]);
            model.setProductXPATH(csvData.get(i)[4]);
            model.setProductPageUrl(csvData.get(i)[5]);
            model.setAddToBasketXPATH(csvData.get(i)[6]);
            model.setDeleteProductInBasketXPATH(csvData.get(i)[7]);
            dp[i][0] = model;
        }
        return dp;
    }


    @Test(dataProvider = "CSVAddToShoppingBasketDataProvider")
    /**
     * Method for running shopping basket tests using test data from a CSV data provider.
     * @param   sbm a model that is used for this scenario.
     * @see     ShoppingBasketModel
     */
    public void addToBasketTest(ShoppingBasketModel sbm) {
        driver.navigate().to("https://www.evomag.ro");
        MainPage mp = PageFactory.initElements(driver, MainPage.class);

        WebElement category = driver.findElement(By.linkText(sbm.getCategory()));

        category.click();
        Assert.assertEquals(driver.getCurrentUrl(), sbm.getCategoryPageUrl());
        System.out.println(sbm.getCategoryPageUrl());

        WebElement subCategory = driver.findElement(By.xpath(sbm.getSubcategory()));
        System.out.println(sbm.getSubcategory());

        subCategory.click();
        Assert.assertEquals(driver.getCurrentUrl(), sbm.getSubcategoryPageUrl());

        System.out.println("product xpath=" + sbm.getProductXPATH());
        WebElement product = driver.findElement(By.xpath(sbm.getProductXPATH()));

        product.click();
        Assert.assertEquals(driver.getCurrentUrl(), sbm.getProductPageUrl());

        WebElement addToBasket = driver.findElement(By.xpath(sbm.getAddToBasketXPATH()));
        addToBasket.click();

        driver.navigate().to("https://www.evomag.ro/cart/sendOrder");

        Assert.assertTrue(driver.findElements(By.xpath(sbm.getDeleteProductInBasketXPATH())).size() != 0);
    }
}
