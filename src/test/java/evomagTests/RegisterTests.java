package evomagTests;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import models.RegistrationModel;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.BaseTest;
import utils.TestListener;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Class for testing register scenarios.
 * @author Mihai Nicolae, 07-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class RegisterTests extends BaseTest {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    ExtentReports ex =  new ExtentReports("D:\\report_" + sdf.format(timestamp) + ".html", false);

    @DataProvider(name = "RegistrationDataProvider")
    /**
     * Basic data provider for tests.
     */
    public Iterator<Object[]> registrationDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", "", "", "Numele trebuie sa contina minim 5 caractere!\n" +
                                        "Adresa de e-mail invalida!\n" +
                                        "Parola trebuie sa contina minim 5 caractere."});
        dp.add(new String[] {"Ivan Ivanovici","", "", "Adresa de e-mail invalida!\n" +
                                        "Parola trebuie sa contina minim 5 caractere."});
        dp.add(new String[] {"Mihai Nicolae", "abc", "", "Adresa de e-mail invalida!\n" +
                                        "Parola trebuie sa contina minim 5 caractere."});
        dp.add(new String[] {"Mihai Nicolae", "abc@", "12345", "Adresa de e-mail invalida!\n"});
        dp.add(new String[] {"Mihai Nicolae", "@abc", "12345", "Adresa de e-mail invalida!\n"});
        dp.add(new String[] {"Vasile Vasilescu","vasile@gmail.com", "1234", "Parola trebuie sa contina minim 5 caractere."});
        return dp.iterator();
    }

    @DataProvider(name = "CSVRegistrationDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        //List<File> files = getListOfFiles("src\\main\\resources\\datasource", "csv");
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("registrationData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            RegistrationModel model = new RegistrationModel();
            model.setName(csvData.get(i)[0]);
            model.setEmail(csvData.get(i)[1]);
            model.setPassword(csvData.get(i)[2]);
            model.setExpectedError(csvData.get(i)[3]);
            dp[i][0] = model;
        }
        return dp;
    }


    @Test(dataProvider = "RegistrationDataProvider")
    /**
     * Method for executing register negative scenarios using data from the basic data provider.
     * @param   name            full name to be sent to the corresponding field in the page
     * @param   email           email address that is used in the registration page
     * @param   password        password string used in registration page
     * @param   expectedMessage expected error message to be compared to the actual error message
     */
    public void registrationTest(String name, String email, String password, String expectedMessage) {
        ExtentTest t1 = ex.startTest("Registration test");

        driver.navigate().to("https://www.evomag.ro/client/auth");
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

        lp.register(name, email, password);

        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());

        String message = alert.getText();

        alert.accept();

        Assert.assertEquals(message, expectedMessage);

        if (message.equals(expectedMessage)) {
            t1.log(LogStatus.PASS, "The correct error message is displayed.");
        } else {
            t1.log(LogStatus.FAIL, "The expected error message is not present.");
        }

        ex.endTest(t1);
        ex.flush();
    }

    @Test(dataProvider = "CSVRegistrationDataProvider")
    /**
     * Method for running registration negative tests using test data from a CSV data provider.
     * The test scenario verifies that the correct error messages are displayed in case of
     * empty or incorrect values entered in registration fields.
     * @param   rm  a model for registration page
     * @see     RegistrationModel
     */
    public void registrationTestCSV(RegistrationModel rm) {
        driver.navigate().to("https://www.evomag.ro/client/auth");
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

        lp.register(rm.getName(), rm.getEmail(), rm.getPassword());

        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());

        String message = alert.getText();

        alert.accept();

        Assert.assertEquals(message.replaceAll("(\\r|\\n)", ""), rm.getExpectedError());
    }

}
