package evomagTests;

import models.LoginModel;
import models.ProductCategoriesModel;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.BaseTest;

import java.io.File;
import java.sql.*;
import java.util.List;

/**
 * Class for login tests including positive and negative scenarios.
 * Also a SQL data provider is included
 * @author Mihai Nicolae, 06-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginTests extends BaseTest {
    @Parameters({"username", "password", "database"})
    @Test
    /**
     * Method for executing login positive scenario using credentials from the database.
     * Database connection credentials are taken from the configuration file (pom.xml).
     * @param   username    username for database connection
     * @param   password    password used for database connection
     * @param   database    name of the database where test data can be found
     */
    public void loginTest(String username, String password, String database) {
        String mainPageURL = "https://www.evomag.ro/";
        String blackFridayURL = "https://www.evomag.ro/frontendCampaign/index";
        String loginPageURL = "https://www.evomag.ro/client/auth";

        //Variable for monitoring the login state
        boolean successfulLogin = false;

        System.out.println("Login with user " + username + ", password " + password + " on DB " + database);
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" +database+ "?useSSL=false&serverTimezone=UTC", username, password);
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from logindata;");

            //For each entry in the result set (row in the database table login method is called using email and password retrieved from the database
            if (res.next()) {
                driver.navigate().to("https://www.evomag.ro/client/auth");
                LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

                lp.login(res.getString("email"), res.getString("password"));

                String currentURL = driver.getCurrentUrl();

                System.out.println(currentURL);

                //If the URL after calling login method is the expected one, successfulLogin variable is set to true
                if (currentURL.equals(mainPageURL) || currentURL.equals(blackFridayURL)) {
                    successfulLogin = true;
                }

                Assert.assertTrue(successfulLogin);

                //Loging out after a successful login and verifying that logout works
                if (successfulLogin) {
                    driver.navigate().to("https://www.evomag.ro/");
                    WebElement we = driver.findElement(By.xpath("//SPAN[text()='Bine ai venit']"));
                    Actions actions = new Actions(driver);
                    actions = actions.moveToElement(we);
                    Action action = actions.build();
                    action.perform();

                    WebElement logoutBtn = driver.findElement(By.xpath("//A[@rel='nofollow'][text()='Logout']"));

                    Actions act = new Actions(driver);
                    act.click(logoutBtn).perform();

                    String url = driver.getCurrentUrl();

                    Assert.assertEquals(url, loginPageURL);
                }

            } else {
                Assert.fail("No login data available");
            }

            res.close();
            conn.close();
        }
        catch (SQLException sexc) {
            sexc.printStackTrace();
            Assert.fail("Could not connect to database.");
        }
    }


    @DataProvider(name = "SQLLoginDataProvider")
    /**
     * Data provider for login negative scenarios, using data email and password data from the database
     */
    public Object[][] sqlDataProvider() {
        int numberOfEntries = 0;
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + database + "?useSSL=false&serverTimezone=UTC", username, password);
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from incompletelogindata;");

            while (res.next()) {
                numberOfEntries++;
            }
            res.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        Object[][] dp = new Object[numberOfEntries][1];

        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/" + "testdata" + "?useSSL=false&serverTimezone=UTC", "testuser", "test");
            Statement stm = conn.createStatement();
            ResultSet res = stm.executeQuery("Select * from incompletelogindata;");

            int i = 0;
            while (res.next()) {
                LoginModel model = new LoginModel();
                model.setEmail(res.getString("email"));
                model.setPassword(res.getString("password"));
                model.setExpectedError(res.getString("expectedError"));
                dp[i][0] = model;
                i++;
            }
            res.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp;
    }


    @Test(dataProvider = "SQLLoginDataProvider")
    /**
     * Method for running login negative tests using test data from a SQL data provider.
     * The test scenario verifies that the correct error messages are displayed in case of
     * empty or incorrect values entered in login fields.
     * @param   lm  a model for login page
     * @see     LoginModel
     */
    public void loginTestNegative(LoginModel lm) {
        driver.navigate().to("https://www.evomag.ro/client/auth");
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);

        lp.login(fixNullString(lm.getEmail()), fixNullString(lm.getPassword()));

        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());

        String message = alert.getText();

        alert.accept();
        //Verifying that the actual error message is the expected one
        Assert.assertEquals(message.replaceAll("(\\r|\\n)", ""), lm.getExpectedError());
    }
}

