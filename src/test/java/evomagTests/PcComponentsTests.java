package evomagTests;

import models.ProductCategoriesModel;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.PcComponentsPage;
import utils.BaseTest;

import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Class for testing PC Components page.
 * @author Mihai Nicolae, 04-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class PcComponentsTests extends BaseTest {
    @DataProvider(name = "CSVPcComponentsDataProvider")
    /**
     * Data provider for tests using data from a .csv file and a generic model for the tested page.
     */
    public Object[][] csvDataProvider() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        System.out.println(this.getClass().getClassLoader().getResource("").getPath());
        File csvFile = new File(classLoader.getResource("pcComponentsTestData.csv").getFile());
        List<String[]> csvData = utils.CSVReader.readCSVFile(csvFile);
        Object[][] dp = new Object[csvData.size()][1];

        for (int i = 0; i < dp.length; i++) {
            ProductCategoriesModel model = new ProductCategoriesModel();
            model.setWebElementName(csvData.get(i)[0]);
            model.setExpectedUrl(csvData.get(i)[1]);
            dp[i][0] = model;
        }
        return dp;
    }

    @Test(dataProvider = "CSVPcComponentsDataProvider")
    /**
     * Method for running tests on PC Components page using test data from a CSV data provider
     * @param   pcm a generic model that is used for this page
     * @see     ProductCategoriesModel
     */
    public void testComponents(ProductCategoriesModel pcm) {
        driver.navigate().to("https://www.evomag.ro/Componente-PC/");
        PcComponentsPage pc = PageFactory.initElements(driver, PcComponentsPage.class);

        Object obj = pc;

        for (Field field : pc.getClass().getFields()) {
            System.out.println(field.getName());
            System.out.println(field.getType());
            System.out.println(WebElement.class);
            if (field.getName().equals(pcm.getWebElementName()) && (field.getType() == WebElement.class)) {
                try {
                    WebElement el = (WebElement) field.get(obj);
                    pc.elementClick(el);
                    String currentURL = driver.getCurrentUrl();
                    Assert.assertEquals(currentURL, pcm.getExpectedUrl());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
