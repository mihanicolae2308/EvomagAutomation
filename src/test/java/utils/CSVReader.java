package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that provides a method for retrieving data from a .csv file
 * @author Mihai Nicolae, 31-Oct-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class CSVReader {
    /**
     * Method for retrieving data from a .csv file
     * @param   file the file to be read.
     * @return  a list of string arrays corresponding to the data for each line in the .csv file
     */
    public static List<String[]> readCSVFile(File file) {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        List<String[]> text = new ArrayList<String[]>();

        try {

            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                text.add(line.split(cvsSplitBy, -1));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }
}
