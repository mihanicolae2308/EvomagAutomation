package utils;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

/**
 * Base class for tests.
 * It contains fields and methods to be used in the test classes derived from this class.
 * @author Mihai Nicolae, 10-Oct-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class BaseTest {
    public String username;
    public String password;
    public String database;

    public WebDriver driver = null;

    WebBrowser.Browsers browserType;

    @BeforeTest()
    /**
     * Method for starting a web browser driver.
     * It is intended to be run before each test.
     */
    public void startDriver() {
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.CHROME);
        } catch (Exception ex) {
            System.out.println("Browser specified is not on the supported browser list! Please use firefox, chrome, ie or htmlunit as argument!");
        }
    }
    @Parameters({"username", "password", "database"})
    @BeforeTest
    /**
     * Method for getting database login credentials from the configuration file,
     * so that they are available for the data providers using data from the test data database.
     * @param u database login username.
     * @param p database login password.
     * @param d database to be connected.
     */
    public void getDBCredentials(String u, String p, String d) {
        username=u;
        password=p;
        database=d;
    }

    @AfterTest
    /**
     * This method is intended to run after each test in order to close the web browser driver.
     */
    public void closeDriver() {
        if (driver != null) {
            driver.close();
        } else {
            System.out.println("No driver to be closed.");
        }
    }

    /**
     * Method used for the situations when test data retrieved from the database for a specific
     * field is null, but the methods that need this data as parameters do not accept null.
     * In this case null is replaced by an empty string ("").
     * @param   str the string that needs to be converted
     * @return  an empty string if the parameter is null or the original string if it is not null.
     */
    public String fixNullString(String str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    }

}