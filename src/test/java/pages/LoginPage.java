package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Class that defines a login page object.
 * It defines the web elements in the page and login and register methods.
 * @author  Mihai Nicolae, 10-Oct-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginPage {

    @FindBy(how = How.ID, using = "RegisterClientForm_FullName")
    private WebElement fullNameField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Password")
    private WebElement passwordField;

    @FindBy(how = How.XPATH, using = "(//INPUT[@class='butn_form'])[1]")
    private WebElement submitValidateField;

    @FindBy(how = How.ID, using = "LoginClientForm_Email")
    private WebElement emailLoginField;

    @FindBy(how = How.ID, using = "LoginClientForm_Password")
    private WebElement passwordLoginField;

    @FindBy(how = How.XPATH, using = "//INPUT[@onclick='login_from_cart()']")
    private WebElement submitValidateLoginField;

    /**
     * Method for performing register action.
     * After completing name, email and password fields the register button is clicked.
     * @param name      the string to be sent to full name field.
     * @param email     the string to be sent to email filed.
     * @param password  the string to be sent to password field.
     */
    public void register(String name, String email, String password) {
        fullNameField.clear();
        fullNameField.sendKeys(name);
        emailField.clear();
        emailField.sendKeys(email);
        passwordField.clear();
        passwordField.sendKeys(password);
        submitValidateField.click();
    }

    /**
     * Method for performing login action.
     * After introducing email and password in the corresponding fields, the login button is clicked.
     * @param loginEmail    email string to be sent to email field.
     * @param loginPassword password string to be sent to password field.
     */
    public void login(String loginEmail, String loginPassword) {
        emailLoginField.clear();
        emailLoginField.sendKeys(loginEmail);
        passwordLoginField.clear();
        passwordLoginField.sendKeys(loginPassword);
        submitValidateLoginField.click();
    }












}
