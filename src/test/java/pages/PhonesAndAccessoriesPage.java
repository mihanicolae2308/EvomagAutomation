package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Class that defines an object corresponding to Phones and Accessories page.
 * It defines the web elements in the page and a method for clicking them.
 * @author  Mihai Nicolae, 05-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class PhonesAndAccessoriesPage {
    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[1]")
    public WebElement mobilePhones;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[2]")
    public WebElement smartwatches;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[3]")
    public WebElement seniorPhones;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[4]")
    public WebElement wiredPhones;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[5]")
    public WebElement telephonicDevices;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[6]")
    public WebElement walkieTalkie;

    @FindBy(how = How.XPATH, using = "(//LI[@class='HasProductMain '])[1]")
    public WebElement phoneAccessories;

    @FindBy(how = How.XPATH, using = "(//LI[@class='HasProductMain '])[2]")
    public WebElement smartWatchAccessories;

    /**
     * Method for performing the click action on a web element in the page.
     * @param we    web element to be clicked.
     */
    public void elementClick(WebElement we){
        we.click();
    }
}
