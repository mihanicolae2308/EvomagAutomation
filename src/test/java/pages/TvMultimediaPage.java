package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Class that defines an object corresponding to TV and Multimedia page.
 * It defines the web elements in the page and a method for clicking them.
 * @author  Mihai Nicolae, 05-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class TvMultimediaPage {
    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[1]")
    public WebElement ledTvs;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[2]")
    public WebElement accessoriesSmartTv;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[3]")
    public WebElement professionalTvSolutions;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[4]")
    public WebElement multimediaPlayers;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[5]")
    public WebElement dvdPlayers;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[6]")
    public WebElement graphicTablets;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[7]")
    public WebElement mp3mp4Players;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[8]")
    public WebElement recorders;

    /**
     * Method for performing the click action on a web element in the page.
     * @param we    web element to be clicked.
     */
    public void elementClick(WebElement we){
        we.click();
    }
}

