package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Class that defines the web elements to be tested in the main page
 * and a method for going to the product categories.
 * @author Mihai Nicolae, 10-Nov-17
 * @version 0.0.1
 * @since   0.0.1
 */
public class MainPage {
    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Laptopuri si Tablete']")
    public WebElement laptopsAndTabletsCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Telefoane mobile si Accesorii']")
    public WebElement mobilePhonesCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][contains(text(),'TV ')][contains(text(),'TV ')])[1]")
    public WebElement tvCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Componente PC']")
    public WebElement pcComponentsCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Sisteme PC']")
    public WebElement pcSystemsCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Monitoare']")
    public WebElement displaysCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Electrocasnice']")
    public WebElement electronicsCategory;

    @FindBy(how = How.XPATH, using = "//A[@class='maintainHover'][text()='Imprimante']")
    public WebElement printersCategory;

    public void goToCategory(WebElement we) {
        we.click();
    }
}
