package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Class that defines an object corresponding to PC Components page.
 * It defines the web elements in the page and a method for clicking them.
 * @author  Mihai Nicolae, 04-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class PcComponentsPage {
    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[1]")
    public WebElement processors;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[2]")
    public WebElement motherboards;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[3]")
    public WebElement videoCards;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[4]")
    public WebElement memories;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[5]")
    public WebElement ssd;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[6]")
    public WebElement mice;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[7]")
    public WebElement mousePads;

    @FindBy(how = How.XPATH, using = "(//LI[@class=' '])[8]")
    public WebElement powerSources;

    /**
     * Method for performing the click action on a web element in the page.
     * @param we    web element to be clicked.
     */
    public void elementClick(WebElement we){
        we.click();
    }
}
