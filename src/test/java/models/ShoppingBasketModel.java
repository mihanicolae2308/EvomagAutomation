package models;

/**
 * Class that defines a model to be used in data providers and tests for shopping basket tests.
 * @author  Mihai Nicolae, 10-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class ShoppingBasketModel {
    private String category;
    private String categoryPageUrl;
    private String subcategory;
    private String subcategoryPageUrl;
    private String productXPATH;
    private String productPageUrl;
    private String addToBasketXPATH;
    private String deleteProductInBasketXPATH;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryPageUrl() {
        return categoryPageUrl;
    }

    public void setCategoryPageUrl(String categoryPageUrl) {
        this.categoryPageUrl = categoryPageUrl;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getSubcategoryPageUrl() {
        return subcategoryPageUrl;
    }

    public void setSubcategoryPageUrl(String subcategoryPageUrl) {
        this.subcategoryPageUrl = subcategoryPageUrl;
    }

    public String getProductXPATH() {
        return productXPATH;
    }

    public void setProductXPATH(String productXPATH) {
        this.productXPATH = productXPATH;
    }

    public String getAddToBasketXPATH() {
        return addToBasketXPATH;
    }

    public void setAddToBasketXPATH(String addToBasketXPATH) {
        this.addToBasketXPATH = addToBasketXPATH;
    }

    public String getProductPageUrl() {
        return productPageUrl;
    }

    public void setProductPageUrl(String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    public String getDeleteProductInBasketXPATH() {
        return deleteProductInBasketXPATH;
    }

    public void setDeleteProductInBasketXPATH(String deleteProductInBasketXPATH) {
        this.deleteProductInBasketXPATH = deleteProductInBasketXPATH;
    }
}

