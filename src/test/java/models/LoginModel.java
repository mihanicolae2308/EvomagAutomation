package models;

/**
 * Class that defines a login model to be used in login data providers and login tests
 * @author  Mihai Nicolae, 07-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class LoginModel {
    private String email;
    private String password;
    private String expectedError;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExpectedError() {
        return expectedError;
    }

    public void setExpectedError(String expectedError) {
        this.expectedError = expectedError;
    }
}
