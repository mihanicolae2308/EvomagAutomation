package models;

import org.openqa.selenium.WebElement;

/**
 * Class that defines a model to be used in data providers and tests for various product pages
 * @author  Mihai Nicolae, 05-Nov-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class ProductCategoriesModel {
    private String webElementName;
    private String expectedUrl;

    public String getWebElementName() {
        return webElementName;
    }

    public void setWebElementName(String webElementName) {
        this.webElementName = webElementName;
    }

    public String getExpectedUrl() {
        return expectedUrl;
    }

    public void setExpectedUrl(String expectedUrl) {
        this.expectedUrl = expectedUrl;
    }
}
