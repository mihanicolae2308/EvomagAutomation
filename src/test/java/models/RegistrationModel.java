package models;

/**
 * Class that defines a registration model to be used in registration data providers and registration tests
 * @author  Mihai Nicolae, 31-Oct-17.
 * @version 0.0.1
 * @since   0.0.1
 */
public class RegistrationModel {
    private String name;
    private String email;
    private String password;
    private String expectedError;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExpectedError() {
        return expectedError;
    }

    public void setExpectedError(String expectedError) {
        this.expectedError = expectedError;
    }
}
